(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(when (< emacs-major-version 27)
  (package-initialize))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(unless (package-installed-p 'nord-theme)
  (package-refresh-contents)
  (package-install 'nord-theme))

;; add to path
(add-to-list 'load-path (concat user-emacs-directory  "lisp"))

;; sync PATH to shell PATH when started from gui
(defun set-exec-path-from-shell-PATH ()
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string
			  "[ \t\n]*$" "" (shell-command-to-string
					  "$SHELL --login -c 'echo $PATH'"
					  ))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))

(set-exec-path-from-shell-PATH)

;; if run as daemon, require every package to be loaded
(if (daemonp)
    (setq use-package-always-demand t))
;;;;;;;;;;;;;;;;;;;;;;;;;
;; use config.org file as source
(org-babel-load-file (concat user-emacs-directory "config.org"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(cursor-type 'bar)
 '(custom-enabled-themes '(nord))
 '(custom-safe-themes
   '("37768a79b479684b0756dec7c0fc7652082910c37d8863c35b702db3f16000f8" default))
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(neo-hide-cursor t)
 '(package-selected-packages
   '(auctex pdf-tools flycheck centaur-tabs sr-speedbar magit yasnippet-snippets whitespace-cleanup-mode which-key use-package sudo-edit sublimity spaceline smex smart-tabs-mode rustic rust-mode rainbow-mode rainbow-delimiters projectile org-bullets nord-theme nhexl-mode neotree mode-line-bell lsp-ui ido-vertical-mode hungry-delete dumb-jump diminish dashboard company-irony caml beacon avy auto-highlight-symbol all-the-icons))
 '(whitespace-style
   '(face trailing tabs spaces lines newline empty indentation space-before-tab space-mark tab-mark newline-mark)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "nil" :family "MesloLGS NF")))))
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
